# Python port of macopt, David MacKay's conjugate gradient optimizer

For documentation and examples, see [here](https://macopt.readthedocs.io/en/latest/).

For further reference, see David MacKay's [original page]( http://www.inference.org.uk/mackay/c/macopt.html ).
